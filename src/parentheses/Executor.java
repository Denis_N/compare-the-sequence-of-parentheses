package parentheses;

/**
 * starts the program
 */

public class Executor {
    private Verification verification;

    Executor( Verification verification) {
        this.verification = verification;
    }

    public static void main(String[] args) {
        Executor executor = new Executor(new Verification());
        executor.verification.checkInputParentheses();
    }
}