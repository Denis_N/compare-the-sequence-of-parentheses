package parentheses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * reads data from the console
 */

class InputData implements ReadDataImp {
    private BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private String inputString;

    @Override
    public String readingData(String inputString) {
       try {
          inputString = bufferedReader.readLine();
          System.out.println(inputString);
          bufferedReader.close();
       } catch (IOException e1) {
           System.out.println("Sorry!!. Something broke");
           e1.printStackTrace();
       }
       return inputString;
    }

    public String getInputString() {
        return inputString;
    }
}