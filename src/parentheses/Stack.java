package parentheses;

/**
 * Partial implementation of the Stack class
 */
class Stack {
    private int maxSizeInputString;
    private int[] arrayStack;
    private int topElement;
    private final int ARRAY_IS_EMPTY = -1;

    Stack(int lengthString) {
        this.maxSizeInputString = lengthString;
        this.arrayStack = new int[maxSizeInputString];
        topElement = ARRAY_IS_EMPTY;
    }

    /**
     * a method that adds an element to the top stack
     */
    void addElement(int element) {
        arrayStack[++topElement] = element;
    }

    /**
     * a method that removes an element from the top position of the stack
     */
    int deleteElement() {
        return arrayStack[topElement--];
    }

     /**
     * a method that checks the stack for emptiness
     */
    boolean isEmpty() {
        return (topElement == ARRAY_IS_EMPTY);
    }
}