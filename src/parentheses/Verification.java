package parentheses;

/**
 * Compare the sequence of parentheses in the stack
 */
class Verification {
    private InputData inputData;
    private Stack stack;
    private int inputStringLength;
    private String stringData;

    Verification() {
        this.inputData = new InputData();
        this.stringData = inputData.readingData(inputData.getInputString());
        this.inputStringLength = stringData.length();
        this.stack = new Stack(inputStringLength);
    }

    void checkInputParentheses() {
        int ZERO = 0;

        for (int INDEX = ZERO; INDEX < inputStringLength; INDEX++) {
            char currentCharacter = stringData.charAt(INDEX);
            switch (currentCharacter) {
                case '{':
                case '[':
                case '(':
                    stack.addElement(currentCharacter);
                    break;
                case '}':
                case ']':
                case ')':
                    if (!stack.isEmpty()) {
                        char characterClosed = (char) stack.deleteElement();
                        if ((currentCharacter == '}' && characterClosed != '{') ||
                                (currentCharacter == ']' && characterClosed != '[') ||
                                (currentCharacter == ')' && characterClosed != '('))
                            System.out.println("Error. Incorrect parentheses " + currentCharacter);
                    }
                    break;
            }
        }
        if (!stack.isEmpty()) {
            System.out.println("Error. There is no closing bracket");
        }
    }
}